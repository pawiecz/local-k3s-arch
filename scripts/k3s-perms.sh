#!/bin/sh

KUBEDIR="${HOME}/.kube"
KUBECONFIG="${KUBEDIR}/config"

mkdir -p "$KUBEDIR"
sudo cp /etc/rancher/k3s/k3s.yaml "$KUBECONFIG"
sudo chown vagrant: "$KUBECONFIG"
echo "export KUBECONFIG=${KUBECONFIG}" >> "${HOME}/.bashrc"
